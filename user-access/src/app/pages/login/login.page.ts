import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email;
  senha;

  constructor(
    private nav: NavController
  ) { }

  ngOnInit() {
  }

  login() {
    if(this.email === 'maria@gmail.com' &&
       this.senha === '12345678') {
         console.log('Login realizado com sucesso');
         this.nav.navigateForward('home');
         //redirecionar o usuário para a tela home
    }else {
      console.log('Usuário ou senha incorretos');
    }
  }

  esqueci(){
    this.nav.navigateForward('forgot');
  }

}
